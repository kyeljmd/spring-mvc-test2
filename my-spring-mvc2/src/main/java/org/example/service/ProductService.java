package org.example.service;

import org.example.dto.ProductDTO;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by kyel on 8/27/2015.
 */
@Service
@Transactional
public interface ProductService {

    void save(ProductDTO productDTO);

    ProductDTO findById(int id);
}
