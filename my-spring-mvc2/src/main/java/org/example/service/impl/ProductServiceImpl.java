package org.example.service.impl;

import org.example.dto.FeatureDTO;
import org.example.dto.ProductDTO;
import org.example.models.Feature;
import org.example.models.FeatureDataType;
import org.example.models.Product;
import org.example.repo.FeatureRepo;
import org.example.repo.ProductRepo;
import org.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyel on 8/27/2015.
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private FeatureRepo featureRepo;

    @Autowired
    private ProductRepo productRepo;

    @Override
    public void save(ProductDTO productDTO) {
        Product product = productDTOToModel(productDTO);
        featureRepo.save(product.getFeatures());
        productRepo.save(product);
    }


    @Override
    public ProductDTO findById(int id) {
       return productToProductDTO(productRepo.findOne(id));

    }

    private Product productDTOToModel(ProductDTO productDTO){
        List<Feature> featureList = new ArrayList<Feature>();
        productDTO
                .getFeatures()
                .forEach(s -> featureList.add(featureDTOToModel(s)));

        Product product = new Product(featureList,productDTO.getName());

        return product;
    }

    private Feature featureDTOToModel(FeatureDTO featureDTO){
        return  new Feature(featureDTO.getName(),
                FeatureDataType.valueOf(featureDTO.getDataType()));
    }

    private ProductDTO productToProductDTO(Product product){
        List<FeatureDTO> featureDTOs = new ArrayList<FeatureDTO>();
        product.getFeatures().forEach(f -> featureDTOs.add(featureToFeatureDTO(f)));
        return new ProductDTO(product.getId(),product.getName(),featureDTOs);
    }

    private FeatureDTO featureToFeatureDTO(Feature feature){
        return new FeatureDTO(feature.getId(),feature.getName(),feature.getDataType().toString());
    }
}
