package org.example.controllers;

import org.example.dto.FeatureDTO;
import org.example.dto.ProductDTO;
import org.example.models.Feature;
import org.example.models.FeatureDataType;
import org.example.models.Product;
import org.example.repo.FeatureRepo;
import org.example.repo.ProductRepo;
import org.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyel on 8/27/2015.
 */
@Controller
public class Init {

    @Autowired
    private ProductService productService;

    @PostConstruct
    public  void init (){
        List<FeatureDTO> featureDTOs = new ArrayList<FeatureDTO>();
        featureDTOs.add(new FeatureDTO("NUMERICAL","1"));
        ProductDTO productDTO = new ProductDTO(featureDTOs,"My Awesome Product Name");
        productService.save(productDTO);
    }
}
