package org.example.controllers;

import org.example.models.Feature;
import org.example.models.FeatureDataType;
import org.example.models.Product;
import org.example.repo.FeatureRepo;
import org.example.repo.ProductRepo;
import org.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private FeatureRepo featureRepo;

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private ProductService productService;

    @RequestMapping("/")
    public String greeting(Model model,@RequestParam(required = false,value="id") int id) {
        model.addAttribute("product", productService.findById(id));
        return "index";
    }
}
