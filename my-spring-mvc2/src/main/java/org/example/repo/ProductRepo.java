package org.example.repo;

import org.example.models.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by kyel on 8/27/2015.
 */
public interface ProductRepo extends PagingAndSortingRepository<Product,Integer>{
}
