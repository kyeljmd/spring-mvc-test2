package org.example.repo;

import org.example.models.Feature;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by kyel on 8/27/2015.
 */
public interface FeatureRepo extends PagingAndSortingRepository<Feature,Integer> {
}
