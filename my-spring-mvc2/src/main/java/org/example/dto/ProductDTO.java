package org.example.dto;

import java.util.List;

/**
 * @author kyel
 */
public class ProductDTO {

    private int id;

    private String name;

    private List<FeatureDTO> features;

    public ProductDTO(List<FeatureDTO> features, String name) {
        this.features = features;
        this.name = name;
    }

    public ProductDTO(int id, String name, List<FeatureDTO> features) {
        this.id = id;
        this.name = name;
        this.features = features;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<FeatureDTO> getFeatures() {
        return features;
    }
}
