package org.example.dto;

import java.util.List;

/**
 * @author kyel
 */
public class FeatureDTO {

    private int id;

    private String name;

    private String dataType;

    private List<ProductDTO> products;

    public FeatureDTO(String dataType, String name) {
        this.dataType = dataType;
        this.name = name;
    }

    public FeatureDTO(int id, String dataType, String name) {
        this.id = id;
        this.dataType = dataType;
        this.name = name;
    }

    public FeatureDTO(int id, String name, String dataType, List<ProductDTO> products) {
        this.id = id;
        this.name = name;
        this.dataType = dataType;
        this.products = products;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDataType() {
        return dataType;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }
}
