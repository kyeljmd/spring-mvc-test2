package org.example.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Feature {

    /**
     * Default constructor as required by Hibernate
     */
    public Feature(){}

    @Id
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "data_type")
    @Enumerated(EnumType.STRING)
    private FeatureDataType dataType;

    @ManyToMany(mappedBy="features")
    private List<Product> products;

    public Feature(String name, FeatureDataType dataType) {
        this.name = name;
        this.dataType = dataType;
    }

    public Feature(int id, String name, FeatureDataType dataType) {
        this.id = id;
        this.name = name;
        this.dataType = dataType;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public FeatureDataType getDataType() {
        return dataType;
    }

}
