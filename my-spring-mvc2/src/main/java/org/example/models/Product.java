package org.example.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Product {

    /**
     * Default constructor as required by Hibernate
     */
    public Product(){

    }


    public Product(String name) {
        this.name = name;
    }

    public Product(List<Feature> features, String name) {
        this.features = features;
        this.name = name;
    }

    @Id
    private int id;

    @Column(name = "name")
    private String name;

    /*
      * Set up a uni directional mapping
     */
    @ManyToMany
    @JoinTable(
            name="PROD_X_FEATURES",
            joinColumns={@JoinColumn(name="PROD_ID", referencedColumnName="ID")},
            inverseJoinColumns={@JoinColumn(name="FEATURE_ID", referencedColumnName="ID")})
    private List<Feature> features;

    public Product(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }
}
